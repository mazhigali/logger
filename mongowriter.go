package logger

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoWriter struct {
	client           *mongo.Client
	collection       *mongo.Collection
	connectionString string
	dbname           string
	indexes          []Index
}

func newMongoWriter(conf *ConfMongo) (conn *mongoWriter) {
	conn = new(mongoWriter)
	conn.connectionString = conf.ConnectionString
	conn.dbname = conf.DbName
	conn.createLocalConnection(conf.CollectionName, conf.Indexes)
	return
}

func (mw *mongoWriter) Write(p []byte) (n int, err error) {
	type MongoLog struct {
		Time    time.Time
		Level   string
		Func    string
		Msg     string
		AddInfo bson.M
	}

	origLen := len(p)
	if len(p) > 0 && p[len(p)-1] == '\n' {
		p = p[:len(p)-1] // Cut terminating newline
	}

	//fmt.Printf("TESTTTT: %#v", string(p))

	l := MongoLog{}

	split := bytes.Split(p, []byte("\t"))
	//fmt.Println("SPLIT Begin:")
	for n, _ := range split {
		//fmt.Println("SPLIT:", string(v))
		switch n {
		case 0:
			t, err := time.Parse(time.RFC3339, string(split[0]))
			if err != nil {
				return origLen, fmt.Errorf("Parse Time %v", err)
			}
			//fmt.Println("TIME:", t)
			l.Time = t
		case 1:
			//fmt.Println("LEVE:", string(split[1]))
			r := strings.NewReplacer(
				`[33m`, "",
				`[34m`, "",
				`[31m`, "",
				`[0m`, "",
			)
			lvl := bytes.Replace(split[1], []byte("\u001b"), []byte(""), -1)
			l.Level = r.Replace(string(lvl))
		case 2:
			//fmt.Println("FUNC:", string(split[2]))
			l.Func = string(split[2])
		case 3:
			l.Msg = string(split[3])
		case 4:
			var objmap bson.M
			err = bson.UnmarshalExtJSON(split[4], true, &objmap)
			//fmt.Println("bSON:", objmap)
			l.AddInfo = objmap
		}
	}

	//fmt.Println("STRUCT", l)

	_, err = mw.collection.InsertOne(context.TODO(), l)
	if err != nil {
		return origLen, fmt.Errorf("Insert Mongo %v", err)
	}
	return origLen, nil
}

func (c *mongoWriter) createLocalConnection(collection string, indexes []Index) (err error) {
	log.Println("Connecting to local mongo server - " + collection)

	// Set client options
	clientOptions := options.Client().ApplyURI(c.connectionString)

	// Connect to MongoDB
	c.client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Error occured while creating mongodb connection: ", err.Error())
	}

	// Check the connection
	err = c.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal("Error occured while pinging mongodb connection: ", err.Error())
	}

	log.Println("Connection established to mongo server")

	c.collection = c.client.Database(c.dbname).Collection(collection)
	if c.collection == nil {
		err = errors.New("Collection could not be created, maybe need to create it manually")
	}

	//create indexes if they exist
	if len(indexes) != 0 {
		models := []mongo.IndexModel{}
		for _, i := range indexes {
			keys := bson.D{}
			for _, f := range i.fields {
				keys = append(keys, primitive.E{Key: f, Value: 1})
			}
			indexModel := mongo.IndexModel{
				Keys:    keys,
				Options: options.Index().SetUnique(i.unique),
			}
			models = append(models, indexModel)
		}

		// Specify the MaxTime option to limit the amount of time the operation can run on the server
		opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
		names, err := c.collection.Indexes().CreateMany(context.TODO(), models, opts)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("created indexes %v\n", names)
	}

	return
}

func (c *mongoWriter) closeConnection() {
	err := c.client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal("Error occured while disconnectiong mongodb: ", err.Error())
	}
	log.Println("Connection to MongoDB closed.")
}
